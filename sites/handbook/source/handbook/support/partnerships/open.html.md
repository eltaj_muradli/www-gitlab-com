---
layout: handbook-page-toc
title: Open Partners
description: "Support specific information for open partners"
---

Open - Resellers, integrators and other sales and services partners join the
program in the Open track. Open is for all Partners in DevOps space, or ItaaS
and other adjacent spaces that are committed to investing in their DevOps
practice buildout. Also the Open track is for Partners seeking to develop
customers or just want to learn about Gitlab and participate in the Gitlab
partner community. GitLab Open Partners may or may not be transacting partners,
and can earn products discounts or referral fees.

## Contacting Support

Open Partners contact us via the [support portal](https://support.gitlab.com).
To help them route properly, they use
[this specialized form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000818199).

This should open the ticket in the Zendesk Open Partner form in zendesk. From
here, Zendesk will use the information in the form to change the requester to
that of the Open Partner's customer. This would mean GitLab Support talks to
the Open Partner's customer directly, using their own plan level. 

In cases where an Open Partner wants to be involved in the discussion, they will
need to mention this to us when submitting the ticket. From there, we can add
them as a CC on the ticket.

**Note**: Never associate a customer to an Open Partner's organization, or
vice-versa!

## File uploads

When Open Partners needs to send support files, we have 2 current methods
available to accomodate this:

* Standard ticket uploads (20MB max)
* [Support Uploader](https://about.gitlab.com/support/providing-large-files.html#support-uploader)
